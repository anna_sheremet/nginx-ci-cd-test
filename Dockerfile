FROM nginx:stable-perl

COPY ./index.html /tmp/index.html

RUN apt-get update

RUN apt-get install -y sed

RUN NAME=$(awk -F '=' '/^NAME=/ {print $2}' /etc/os-release) \
    && sed -i "s/{{NAME}}/$(echo $NAME | sed 's/[\/&]/\\&/g')/g" /tmp/index.html

RUN mv /tmp/index.html /usr/share/nginx/html


